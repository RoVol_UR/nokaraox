﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.Gms.Tasks;
using Android.Graphics;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Firebase;
using Firebase.Storage;
using Java.IO;
using Java.Lang;

namespace NotKaraoX.Activity
{
    [Activity(Label = "EventForm")]
    public class EventForm : AppCompatActivity, IOnProgressListener, IOnSuccessListener, IOnFailureListener
    {
        Button btnChoose, btnUpload;
        ImageView imgView;

        private Android.Net.Uri filePath;
        const int PICK_IMAGE_REQUEST = 71;

        ProgressDialog progressDialog;

        FirebaseStorage storage;
        StorageReference storageReference;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.event_form);

            FirebaseApp.InitializeApp(this);
            storage = FirebaseStorage.Instance;
            storageReference = storage.GetReferenceFromUrl("gs://notkaraox.appspot.com ");

            // Create your application here

            imgView = FindViewById<ImageView>(Resource.Id.imgView);
            btnChoose = FindViewById<Button>(Resource.Id.btn_choose);
            btnUpload = FindViewById<Button>(Resource.Id.btn_upload);

            btnChoose.Click += delegate
            {
                ChooseImage();
            };

            btnUpload.Click += delegate
            {
                UploadImage();
            };

        }

        private void UploadImage()
        {
            if (filePath != null)
            {

                progressDialog = new ProgressDialog(this);
                progressDialog.SetTitle("Uploading...");
                progressDialog.Window.SetType(Android.Views.WindowManagerTypes.SystemAlert);

                progressDialog.Show();


                var images = storageReference.Child("images/" + Guid.NewGuid().ToString());
                images.PutFile(filePath).AddOnProgressListener(this)
                    .AddOnSuccessListener(this)
                    .AddOnFailureListener(this);

            }
        }

        void ChooseImage()
        {
            Intent intent = new Intent();
            intent.SetType("image/*");
            intent.SetAction(Intent.ActionGetContent);
            StartActivityForResult(Intent.CreateChooser(intent, "select picture"), PICK_IMAGE_REQUEST);
        }
        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if (requestCode == PICK_IMAGE_REQUEST &&
                resultCode == Result.Ok &&
                data != null &&
                data.Data != null)
            {
                filePath = data.Data;
                try
                {
                    Bitmap bitmap = MediaStore.Images.Media.GetBitmap(ContentResolver, filePath);
                    imgView.SetImageBitmap(bitmap);
                }
                catch (IOException ex)
                {
                    ex.PrintStackTrace();
                }

            }
        }



        public void OnSuccess(Java.Lang.Object result)
        {
            progressDialog.Dismiss();
            Toast.MakeText(this, "Uploaded", ToastLength.Short).Show();
        }

        public void OnFailure(Java.Lang.Exception e)
        {
            progressDialog.Dismiss();
            Toast.MakeText(this, "" + e.Message, ToastLength.Short).Show();

        }

        public void OnProgress(Java.Lang.Object snapshot)
        {
            var taskSnapShot = (UploadTask.TaskSnapshot)snapshot;
            double progress = (100.0 * taskSnapShot.BytesTransferred / taskSnapShot.TotalByteCount);
            progressDialog.SetMessage("Uploaded " + (int)progress + " %");
        }
    }
}