﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace NotKaraoX.Activity
{
    [Activity(Label = "Main_buttons")]
    public class Main_buttons : AppCompatActivity
    {
        Button mButton;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.mainButtons);

            mButton = FindViewById<Button>(Resource.Id.new_page);
            // Create your application here
            mButton.Click += mButton_Click;

            
        }
        void mButton_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(EventForm));
            StartActivity(intent);
        }
    }
}